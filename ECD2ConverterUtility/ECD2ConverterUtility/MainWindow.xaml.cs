﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Diagnostics;
using System.IO;
using Path = System.IO.Path;

namespace ECD2ConverterUtility {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private string Convertedmp4folder = AppDomain.CurrentDomain.BaseDirectory + "Converted_mp4";
        private string DraggedFile = "";
        private bool videoConverted = false;
        private string OutputFile = "";

        public MainWindow() {
            InitializeComponent();
            CopyButton.Visibility = Visibility.Hidden;
            ProgressbarDummy.Visibility = Visibility.Hidden;
            Directory.CreateDirectory(Convertedmp4folder);
            
        }


        private async Task DoAsyncConversion() {
            await Task.Run(() => { videoConverted = ConvertTomp4(); });
        }

        private async void LastFileDragged(object sender, DragEventArgs e)
        {
            statusmsg.Content = "";
            ProgressbarDummy.Visibility = Visibility.Visible;

            List<string> Filetypes = new List<string>() { ".avi", ".flv", ".wmv", ".mp4", ".m4v", ".mpg", ".mpeg4", ".flv1", ".vplayer", ".mov", ".mjpeg"};
            if(e.Data.GetDataPresent(DataFormats.FileDrop)) {
                var dropped = ((string[])e.Data.GetData(DataFormats.FileDrop));
                var files = dropped.ToList();

                if (!files.Any())
                {
                    return;
                }

                //only one file allowed at a time
                this.DraggedFile = files[0];
                string extension = Path.GetExtension(DraggedFile);
                if (!Filetypes.Contains(extension))
                {
                    MessageBox.Show(extension + " not supported", "Warning", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
                    return;
                }

                string startmsg = Path.GetFileName(DraggedFile);
                FilenameLabel.Content = startmsg;
                statusmsg.Content = "conversion in progress..";

                ProgressbarDummy.IsIndeterminate = true;
                await DoAsyncConversion();
                ProgressbarDummy.IsIndeterminate = false;
                ProgressbarDummy.Visibility = Visibility.Hidden;

                if (videoConverted)
                {
                    statusmsg.Content = "Conversion Finished.";
                    CopyButton.Visibility = Visibility.Visible;
                    MessageBox.Show("Conversion Finished");
                }
                else
                {
                    FilenameLabel.Content = "Drag Your File Here...";
                    statusmsg.Content = "Conversion Cancelled.";
                }

                
                
            }
        }

        private string setTheExecutable()
        {
            string tempPathForExe = Path.GetTempPath() + "ffmpegtempconverter.exe";
            if (System.IO.File.Exists(tempPathForExe))
            {
                return tempPathForExe;
            }
            
            byte[] ffmpeg_bytes = Properties.Resources.ffmpeg;
            using (FileStream exeFileStream = new FileStream(tempPathForExe, FileMode.CreateNew))
            {
                exeFileStream.Write(ffmpeg_bytes, 0, ffmpeg_bytes.Length);
            }

            return tempPathForExe;
        }

        private bool ConvertTomp4()
        {
            string outputFileName = Convertedmp4folder + "\\"  + Path.GetFileNameWithoutExtension(DraggedFile) + ".mp4".Trim();
            if (File.Exists(outputFileName))
            {
                MessageBoxResult result = MessageBox.Show(Path.GetFileNameWithoutExtension(DraggedFile) + ".mp4 already exists. Do you want to overwrite it?","Confirmation", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.No)
                {
                    return false; 
                }
            }

            File.Delete(outputFileName);

            string cmd = "-i " + " " + DraggedFile + " " + outputFileName;
            var proc = new Process();
            proc.StartInfo.FileName = setTheExecutable();
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.Arguments = cmd;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.Start();
            proc.WaitForExit();
            var exitCode = proc.ExitCode;
            proc.Close();
            this.OutputFile = outputFileName;
            return true;

        }

        private void CopyButton_Clicked(object sender, RoutedEventArgs e) {
            Clipboard.SetText(this.OutputFile);
        }
    }
}
